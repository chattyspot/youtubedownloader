package com.zavanton.yoump3.di.qualifier.channel

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class LowNotificationChannel