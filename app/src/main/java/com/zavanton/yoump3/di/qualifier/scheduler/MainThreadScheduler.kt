package com.zavanton.yoump3.di.qualifier.scheduler

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MainThreadScheduler