package com.zavanton.yoump3.ui.download.view

interface IDownloadService {

    fun startForeground()
    fun stopForeground()
}