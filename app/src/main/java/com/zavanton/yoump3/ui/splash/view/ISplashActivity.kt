package com.zavanton.yoump3.ui.splash.view

interface ISplashActivity {

    fun proceedWithApp()
    fun repeatRequestPermissions()

    fun onPositiveButtonClick()
    fun onNegativeButtonClick()
}
